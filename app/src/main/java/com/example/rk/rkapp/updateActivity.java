package com.example.rk.rkapp;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class updateActivity extends AppCompatActivity {
    private EditText srchh,uname,mobl,due;
    private Button search,save;
    userDbHelper userDbHelper;
    SQLiteDatabase sqLiteDatabase;
    String searchbyname,newname,newmob,newdue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        srchh = (EditText)findViewById(R.id.srch);
        uname = (EditText)findViewById(R.id.etname);
        mobl = (EditText)findViewById(R.id.etphn);
        due = (EditText)findViewById(R.id.etdue);
        search = (Button)findViewById(R.id.btnsrch);
        save = (Button)findViewById(R.id.btnsend);
        uname.setVisibility(View.GONE);
        mobl.setVisibility(View.GONE);
        due.setVisibility(View.GONE);

    }
    public void search(View view){
        searchbyname = srchh.getText().toString();
        userDbHelper = new userDbHelper(getApplicationContext());
        Cursor cursor = userDbHelper.getInformation(searchbyname,sqLiteDatabase);
        if(cursor.moveToFirst()){
            newmob = cursor.getString(1);
            newdue = cursor.getString(2);
            newname = searchbyname;
            uname.setText(newname);
            mobl.setText(newmob);
            due.setText(newdue);
            uname.setVisibility(View.VISIBLE);
            mobl.setVisibility(View.VISIBLE);
            due.setVisibility(View.VISIBLE);
        }
        userDbHelper.close();
    }
    public void update(View view){
        userDbHelper = new userDbHelper(getApplicationContext());
        //sqLiteDatabase = userDbHelper.getWritableDatabase();
        String name,mobil,due1;
        name = uname.getText().toString();
        mobil = mobl.getText().toString();
        due1 = due.getText().toString();
        int count = userDbHelper.updateInfo(searchbyname,name,mobil,due1,sqLiteDatabase);
        Toast.makeText(getApplicationContext(),count+"updated",Toast.LENGTH_LONG).show();
        userDbHelper.close();

    }



}
