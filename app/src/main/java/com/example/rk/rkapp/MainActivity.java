package com.example.rk.rkapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText name;
    private EditText password;
    private Button login;
    private TextView info;
    private int counter =5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = (EditText)findViewById(R.id.eTuname);
        password = (EditText)findViewById(R.id.eTpwd);
        login = (Button) findViewById(R.id.btnlogin);

        info = (TextView)findViewById(R.id.tvinfo);
        info.setText("no of attempts remaining : 5");

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate(name.getText().toString(),password.getText().toString());
            }
        });
    }
    private void validate(String username,String password)
    {
        if(username.equals("bharath") && password.equals("8019979908") || username.equals("srikanth") && password.equals("1234567890") || username.equals("jaffer") && password.equals("0123456789") || username.equals("ravikanth") && password.equals("1357908642") || username.equals("manjunath") && password.equals("0246813579")){
            Intent intent = new Intent(MainActivity.this,Dashboard.class);
            startActivity(intent);
        }
        else{
            counter--;
            info.setText("no of attempts remaining" + String.valueOf(counter));
            if(counter == 0 ){
                login.setEnabled(false);
            }
        }
    }
}
