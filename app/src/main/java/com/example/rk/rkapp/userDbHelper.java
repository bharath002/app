package com.example.rk.rkapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by user on 11/27/2017.
 */

public class userDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "USERINFO.DB";
    private static final int DATABASE_VERSION = 1;
    private static final String CREATE_TABLE="CREATE TABLE "+usercontract.NewUserInfo.TABLE+"("+usercontract.NewUserInfo.USER_NAME+" TEXT,"+usercontract.NewUserInfo.PHONE+" TEXT,"+usercontract.NewUserInfo.Capital+" TEXT)" ;
    //private static final  String UPDATE_RECORD ="UPDATE";
    public userDbHelper(Context context)
    {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
        //Log.e("DATABASE OPERATIONS","DATABASE created/opened ...");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE);
        Log.e("DATABASE OPERATIONS","Table created/opened ...");

    }
    public void addInformation(String name,String mob,String due,SQLiteDatabase db){
        ContentValues contentValues = new ContentValues();
        contentValues.put(usercontract.NewUserInfo.USER_NAME,name);
        contentValues.put(usercontract.NewUserInfo.PHONE,mob);
        contentValues.put(usercontract.NewUserInfo.Capital,due);
        db.insert(usercontract.NewUserInfo.TABLE,null,contentValues);
        //Toast.makeText(Register.this,String.valueOf(id),Toast.LENGTH_LONG).show();
        Log.e("DATABASE OPERATIONS","One new row inserted ...");
    }
    public Cursor getInformation(String s,SQLiteDatabase db)
    {
        Cursor cursor;
        db = this.getReadableDatabase();
        String[] projections = {usercontract.NewUserInfo.USER_NAME, usercontract.NewUserInfo.PHONE, usercontract.NewUserInfo.Capital};
        String selection = usercontract.NewUserInfo.USER_NAME + " = '"+s+"'";
        cursor = db.query(usercontract.NewUserInfo.TABLE,projections,selection,null,null,null,null);
        return cursor;
    }

    public int updateInfo(String oldname,String newname,String phone,String due,SQLiteDatabase sqldb){
        sqldb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(usercontract.NewUserInfo.USER_NAME,newname);
        contentValues.put(usercontract.NewUserInfo.PHONE,phone);
        contentValues.put(usercontract.NewUserInfo.Capital,due);
        String selection = usercontract.NewUserInfo.USER_NAME +"='"+oldname+"'";
        //String[] selection_args = {usercontract.NewUserInfo.USER_NAME};
        int count = sqldb.update(usercontract.NewUserInfo.TABLE,contentValues,selection,null);
        return count;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
