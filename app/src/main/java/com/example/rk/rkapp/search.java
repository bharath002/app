package com.example.rk.rkapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class search extends AppCompatActivity {
    private EditText searchByName;
    private Button srch;
    private Button send;
    private Button update;
    SQLiteDatabase sqLiteDatabase ;
    userDbHelper userDbHelper;
    Cursor cursor;
    String dname= null;
    String dmob = null;
    String ddue1 = null;
    private EditText uname,phone,due;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        searchByName = (EditText)findViewById(R.id.etsearch);
        srch = (Button)findViewById(R.id.btnsrch);
        send = (Button)findViewById(R.id.btnsend);
        uname =(EditText) findViewById(R.id.uname);
        phone =(EditText) findViewById(R.id.phone);
        due =(EditText) findViewById(R.id.due);
        update =(Button)findViewById(R.id.update);


        srch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userDbHelper = new userDbHelper(getApplicationContext());
                sqLiteDatabase = userDbHelper.getReadableDatabase();
                String name,mob,due1;
                cursor = userDbHelper.getInformation(searchByName.getText().toString(),sqLiteDatabase);
                if(cursor.moveToFirst()){
                    do{

                        name = cursor.getString(0);
                        mob = cursor.getString(1);
                        due1 = cursor.getString(2);

                        uname.setText(name);
                        phone.setText(mob);
                        due.setText(due1);
                        dname = uname.getText().toString();
                        dmob = phone.getText().toString();
                        ddue1 = due.getText().toString();
                        Toast.makeText(getBaseContext(),"retrived data",Toast.LENGTH_LONG).show();

                    }while(cursor.moveToNext());

                }

                final String sms = "hello "+dname+" your balance is "+ddue1+" please pay through the following link ...";
                send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(phone.getText().toString(),null,sms,null,null);
                            Toast.makeText(search.this,"sent",Toast.LENGTH_SHORT).show();
                        }catch (Exception e){
                            Toast.makeText(search.this,"failed",Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(search.this,updateActivity.class);
                startActivity(intent);
            }
        });
    }

}
